import cv2

fast = cv2.FastFeatureDetector_create()

vid_cap = cv2.VideoCapture(0)
vid_cap.open(0)

while True:
    _,img = vid_cap.read()

    kp = fast.detect(img, None)
    img_features = cv2.drawKeypoints(image=img, outImage=img, keypoints=kp, color=(255, 0, 0))

    cv2.imshow("Features",img_features)
    cv2.waitKey(1)